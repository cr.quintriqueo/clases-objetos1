/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.cris;

/**
 *
 * @author cr.quintriqueo
 */
public class pizza {
    //paso 1 crear atributos
    private String nombre, tipo, masa;
    //String nombre;
    //String tipo;          hay dos formas de escribir esto
    //String masa;
    
    //paso 2 construir metodos (customer)
    public void preparar(){
        System.out.println("Estamos preparando tu pizza");
    
    }
    public void calentar(){
        System.out.println("Estamos calentando tu pizza");
    
    }
    //paso 4 crear constructor con y sin parametros

    public pizza() {
    }

    public pizza(String nombre, String tipo, String masa) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.masa = masa;
    }
    
    //paso 5 crear metodos setter y getter
    //getter para obtener un dato
    //setter para modificarlo

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMasa() {
        return masa;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }
    
    //paso 6 crar metodo toSTRING

    @Override
    public String toString() {
        return "pizza{" + "nombre=" + nombre + ", tipo=" + tipo + ", masa=" + masa + '}';
    }
    
    
    
    
}
